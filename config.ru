require 'skeksis'
require 'rubytree'

class SkeksisApp
  def initialize
    @skeksis = Skeksis::GemBridge.new
  end

  def call(env)
    status = 200
    headers = { "content-type" => "text/html" }
    body = @skeksis.query(env['PATH_INFO'], env)

    [status, headers, body]
  end

end

run SkeksisApp.new
