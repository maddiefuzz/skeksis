module Skeksis
  class IR
    Header = <<~HTML
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <title>Gembridge</title>
    </head>
    <body>
    HTML

    Footer = <<~HTML
    </body>
    </html>
    HTML

    # TODO: Fix these method arguments so they don't depend on a rack env hash
    def htmlize(env, proxied_uri=nil, port=80)
      @http = URI::HTTP.build(host: env['HTTP_HOST'], port: env['SERVER_PORT'], path: env['PATH_INFO'])
      unless proxied_uri.nil?
        @proxied_uri = URI.parse(proxied_uri)
      end

      content = self.parse_ir.join("\n")
      Header + "#{content}\n" + Footer
    end

    private 
    def parse_ir
      html = self.map do |entry|
        content = lambda { |entry|
          unless entry[:content].nil? then return entry[:content][0] end
        }

        text = content.call(entry)

        case entry[:type]
        when :blank
          "<br>"
        when :header
          level = entry[:level]
          "<h#{level}>#{text}</h#{level}>"
        when :list
          list = entry[:content].map do |textline|
            "<li>" + textline + "</li>"
          end.join("\n")
          "<ul>" + list + "</ul>"
        when :quote
          "<pre>#{text}</pre>"
        when :uri
          uri = entry[:uri]
          text = entry[:text]
          http_link = build_uri(uri)
          "<a href=\"#{http_link}\">#{text}</a>"
        when :verbatim
          "<pre>" + entry[:content].join("\n") + "</pre>"
        when :text
          "<p>#{text}</p>"
        end
      end

      html
    end

    # TODO: Relative and absolute links need to be properly handled, both proxied and non
    def build_uri(link)
      uri = URI.parse(link)

      if uri.relative? then puts "relative" else puts "absolute" end

      if uri.relative?
        path = Pathname.new(@http.path).join(uri.path)
        return URI::HTTP.build(host: @http.host, path: path.to_s, port: @http.port)
      end

      if uri.host == @proxied_uri.host
        return URI::HTTP.build(host: @http.host, path: uri.path, port: @http.port)
      else
        return link
      end
    end
  end
end
