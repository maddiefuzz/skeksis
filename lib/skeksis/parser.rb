# Code adapted from MIT-licensed gemini-rb project
# parser.rb hosted at
# https://github.com/genenetwork/gemini-rb/blob/main/lib/gemini-rb/parser.rb
#
# Internal Representation types
# :blank
# :header
# :list
# :quote
# :uri
# :verbatim
# :text
#

module Skeksis
  class IR < Array
    def strip_markers!
      self.map! do |line|
        type = line[:type]
        content = line[:content]
        text = content[0]

        case type
        when :blank
          #{ type: type, content: nil }
          { type: type }
        when :header
          m = /^(#+)(\s*)(.*)/.match(text)
          level = m[1].count("#")
          { type: type, level: level, content: [m[3]] }
        when :list
          { type: type, content: content.map { |t| t.sub(/^\*\s*/, "") } }
        when :quote
          { type: type, content: content.map { |t| t.sub(/^\>\s?/, "") } }
        when :uri
          a = text.sub(/^=>\s*/, "").split(" ", 2)
          link = a[0]
          text = a[1]
          { type: type, uri: link, text: text }
        when :verbatim
          # TODO: Match with syntax highlighting, maybe 
          content.map! do |line|
            m = /^```(.*)/.match(line)
            unless m.nil?
              nil
            else
              line
            end
          end.compact!
          { type: type, content: content }
        else
          { type: type, content: content }
        end
      end.compact!

      self
    end
  end

  module Parser
    extend self

    def parse(input, strip_blanks=false)
      list = IR.new

      data = input.map(&:chomp)

      in_verbatim_block = false
      in_multiline_block = false
      in_block = nil
      content = []

      data.each do |line|
        type = get_type(line)

        case type
        when :list, :quote
          if not in_verbatim_block
            in_multiline_block = true
            in_block = type
          end
          content.push(line)
        when :verbatim
          if in_verbatim_block == false
            in_verbatim_block = true
            content.push(line)
          elsif in_verbatim_block == true
            in_verbatim_block = false
            content.push(line)
            list.push({ type: :verbatim, content: content })
            content = []
            next
          end
        else
          if in_multiline_block == true
            list.push({ type: in_block, content: content })
            in_multiline_block = false
            content = []
          end
          
          if in_verbatim_block == true
            content.push(line)
          end
        end

        if type == :blank and strip_blanks == true then next end

        if in_verbatim_block == false and in_multiline_block == false
          list.push({ type: type, content: [line] })
        end
      end

      list.strip_markers!
    end

    private

    def get_type(l)
      case l
      when ""
        :blank
      when /^#/
        :header
      when /^\*/
        :list
      when /^\>/
        :quote
      when /^\=>/
        :uri
      when /^```/
        :verbatim
      else
        :text
      end
    end
  end
end
